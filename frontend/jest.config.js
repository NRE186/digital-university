module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  modulePathIgnorePatterns: [
    "src/main.js",
    "src/plugins",
    "src/store",
    "src/router",
    "src/App.vue",
  ],
  collectCoverage: true,
  collectCoverageFrom: ["src/**/*.js", "src/**/*.vue", "!**/node_modules/**"],
  coverageDirectory: "./coverage",
  coverageReporters: ["html", "text", "text-summary"],
}
