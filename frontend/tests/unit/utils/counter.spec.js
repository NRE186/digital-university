import counter from "@/utils/counter.js"

describe("Counter.js", () => {
  it("Test with other values", () => {
    expect(counter(1, ["элемент", "элемента", "элементов"])).toBe("элемент")
    expect(counter(2, ["элемент", "элемента", "элементов"])).toBe("элемента")
    expect(counter(10, ["элемент", "элемента", "элементов"])).toBe("элементов")
    expect(counter(123, ["элемент", "элемента", "элементов"])).toBe("элемента")
  })
})
