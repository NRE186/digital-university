import vuetify from "vuetify"
import Vue from "vue"
import Vuex from "vuex"
import { shallowMount, createLocalVue } from "@vue/test-utils"
import Timetable from "@/views/Timetable.vue"
import mock_data from "./mocks"

describe("Documents.vue", () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  let wrapper
  let getters
  let store

  beforeEach(() => {
    getters = {
      timetableGet: () => mock_data.timetable,
    }

    store = new Vuex.Store({
      getters,
    })
    Vue.use(vuetify)
    wrapper = shallowMount(Timetable, { store, localVue })
  })

  it("Default values", () => {
    expect(wrapper.vm.search).toBe("")
    expect(wrapper.vm.searchStatus).toBe(false)
    expect(wrapper.vm.timetables).toBe(mock_data.timetable)
    expect(wrapper.vm.grades).toStrictEqual(mock_data.grades)
    expect(wrapper.vm.institutes).toStrictEqual(mock_data.institutes)
  })

  describe("Watchers", () => {
    it("Check search with null", () => {
      wrapper.setData({
        search: null,
      })
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.search).toBe("")
      })
    })
    it("Check search", () => {
      wrapper.setData({
        search: "Институт государства и права",
      })
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.filterTimetable()).toStrictEqual(mock_data.timetable)
      })
    })
  })
})
