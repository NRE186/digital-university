import vuetify from "vuetify"
import Vue from "vue"
import { shallowMount } from "@vue/test-utils"
import TestData from "@/views/TestData.vue"
import mock_data from "./mocks"

describe("TestData.vue", () => {
  let wrapper
  beforeEach(() => {
    Vue.use(vuetify)
    wrapper = shallowMount(TestData)
  })

  it("Default values", () => {
    expect(wrapper.vm.items).toEqual(mock_data.testDataItems)
  })
})
