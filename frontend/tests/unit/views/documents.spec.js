import vuetify from "vuetify"
import Vue from "vue"
import Vuex from "vuex"
import { shallowMount, createLocalVue } from "@vue/test-utils"
import Documents from "@/views/Documents.vue"
import mock_data from "./mocks"

describe("Documents.vue", () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  let wrapper
  let getters
  let store

  beforeEach(() => {
    getters = {
      docsGet: () => mock_data.docs,
    }

    store = new Vuex.Store({
      getters,
    })
    Vue.use(vuetify)
    wrapper = shallowMount(Documents, { store, localVue })
  })

  it("Default values", () => {
    expect(wrapper.vm.docs).toBe(mock_data.docs)
    expect(wrapper.vm.search).toBe("")
  })

  it("Empty docs", () => {
    wrapper.setData({
      docs: [],
    })
    expect(wrapper.vm.docs).toStrictEqual([])
  })

  describe("Watchers", () => {
    it("Check search with null", () => {
      wrapper.setData({
        search: null,
      })
      wrapper.vm.$nextTick(() => {
        expect(wrapper.vm.search).toBe("")
      })
    })
  })

  describe("Computed properties", () => {
    it("Filtered search with empty search field", () => {
      expect(wrapper.vm.filteredSearch).toBe(mock_data.docs)
    })
    it("Filtered search with search field", () => {
      wrapper.setData({
        search:
          "01.04.02 - Математическое и информационное обеспечение систем управления деятельностью предприятий нефтегазовой отрасли",
      })
      expect(wrapper.vm.filteredSearch).toStrictEqual([mock_data.docs[1]])
    })
  })
})
