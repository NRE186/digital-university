import vuetify from "vuetify"
import Vue from "vue"
import Vuex from "vuex"
import { shallowMount, createLocalVue } from "@vue/test-utils"
import Login from "@/views/Login.vue"

describe("Login.vue", () => {
  const localVue = createLocalVue()
  localVue.use(Vuex)
  let wrapper
  let actions
  let store
  beforeEach(() => {
    actions = {
      auth: jest.fn(),
    }

    store = new Vuex.Store({
      actions,
    })
    Vue.use(vuetify)
    wrapper = shallowMount(Login, { store, localVue })
  })

  it("Default values", () => {
    expect(wrapper.vm.username).toBe("")
    expect(wrapper.vm.password).toBe("")
    expect(wrapper.vm.valid).toBe(true)
    expect(wrapper.vm.authStatus).toBe(true)
  })

  describe("Methods", () => {
    it("Login - Vuex auth method to have been called", () => {
      wrapper.setData({
        username: "admin",
        password: "admin",
      })
      wrapper.vm.login()
    })
  })
})
