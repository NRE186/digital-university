import vuetify from "vuetify"
import Vue from "vue"
import { shallowMount } from "@vue/test-utils"
import Main from "@/views/Main.vue"

describe("Main.vue", () => {
  let wrapper
  beforeEach(() => {
    Vue.use(vuetify)
    wrapper = shallowMount(Main)
  })

  it("Default values", () => {
    expect(wrapper.vm.paralax).toBe(false)
    expect(wrapper.vm.info).toBe(false)
  })
})
