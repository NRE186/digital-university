import axios from "axios"
import { API } from "@/constants"

export default {
  state: {
    debts: [],
  },
  getters: {
    getDebts: (state) => {
      return state.debts
    },
  },
  actions: {
    fetchDebts({ commit }, data) {
      return axios
        .get(`${API.getDebt}`, {
          params: {
            pk: data.pk,
            group: data.group.name,
          },
        })
        .then((response) => {
          commit("setDebts", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
  },
  mutations: {
    setDebts(state, data) {
      state.debts = data
    },
  },
}
