import axios from "axios"
import { API } from "@/constants"

export default {
  state: {
    lectionScore: [],
    lections: [],
  },
  getters: {
    getLectionScore: (state) => {
      return state.lectionScore
    },
    getLections: (state) => {
      return state.lections
    },
  },
  actions: {
    fetchLectionScore({ commit }, data) {
      return axios
        .get(`${API.getLections}`, {
          params: {
            group: data.group,
            subject: data.subject,
          },
        })
        .then((response) => {
          commit("setLectionScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    updateLectionMark({ commit }, data) {
      axios
        .get(`${API.updateLectionMark}`, {
          params: {
            pk: data.pk,
            mark: data.mark,
          },
        })
        .then((response) => {
          commit("updateLectionScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    createLection({ commit }, data) {
      return axios
        .post(`${API.createLection}`, {
          name: data.name,
          subject: data.subject,
          lection_date: data.lection_date,
          description: data.description,
        })
        .then((response) => {
          commit("updateLectionScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    updateLection({ commit }, data) {
      return axios
        .post(`${API.updateLection}`, {
          pk: data.pk,
          name: data.name,
          lection_date: data.lection_date,
          description: data.description,
        })
        .then((response) => {
          commit("updateLectionScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
  },
  mutations: {
    setLectionScore(state, data) {
      state.lectionScore = data
      let res = []
      data.forEach((el) => {
        res.push({
          pk: el.lection.pk,
          name: el.lection.name,
          description: el.lection.description,
          subject: el.lection.subject,
          lection_date: el.lection.lection_date,
          editable: false,
        })
      })
      let unique = Array.from(new Set(res.map(JSON.stringify))).map(JSON.parse)
      state.lections = unique
    },
    updateLectionScore(state, data) {
      state.lectionScore = data
    },
  },
}
