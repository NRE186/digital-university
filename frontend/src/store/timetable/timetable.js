import timetable from "./timetable.json"

export default {
  state: {
    links: [],
  },
  getters: {
    timetableGet: (state) => {
      return state.links
    },
  },
  actions: {
    getTimetable({ commit }) {
      commit("setTimetable", timetable)
    },
  },
  mutations: {
    setTimetable(state, data) {
      state.links = data
    },
  },
}
