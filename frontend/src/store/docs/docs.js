import docs from "./docs.json"

export default {
  state: {
    links: [],
  },
  getters: {
    docsGet: (state) => {
      return state.links
    },
  },
  actions: {
    getDocs({ commit }) {
      commit("setDocs", docs)
    },
  },
  mutations: {
    setDocs(state, data) {
      state.links = data
    },
  },
}
