import Vue from "vue"
import Vuex from "vuex"
import axios from "axios"
import docs from "./docs/docs"
import timetable from "./timetable/timetable"
import debts from "./debts/debts"
import exam from "./exam/exam"
import works from "./works/works"
import lections from "./lections/lections"
import { API } from "@/constants"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    API_status: "",
    links: [
      {
        name: "На главную",
        link: "/",
        icon: "home",
        disabled: false,
      },
      {
        name: "Расписание",
        link: "/timetable",
        icon: "date_range",
        disabled: false,
      },
      {
        name: "Документы",
        link: "/documents",
        icon: "description",
        disabled: false,
      },
      {
        name: "Тестовые данные",
        link: "/test",
        icon: "bug_report",
        disabled: false,
      },
      { name: "Личный кабинет", link: "/dashboard", icon: "dashboard", disabled: false },
    ],
    user: "",
    group: "",
    role: "",
    position: "",
    authStatus: false,
    token: "",
    pk: "",
    subjects: [],
  },
  getters: {
    getPk: (state) => {
      return state.pk
    },
    getLinks: (state) => {
      return state.links
    },
    getStatus: (state) => {
      return state.API_status
    },
    getUser: (state) => {
      return state.user
    },
    getGroup: (state) => {
      return state.group
    },
    getPosition: (state) => {
      return state.position
    },
    getAuthStatus: (state) => {
      return state.authStatus
    },
    getToken: (state) => {
      return state.token
    },
    getRole: (state) => {
      return state.role
    },
    getSubjects: (state) => {
      return state.subjects
    },
    getFullFIO: (state) => {
      return `${state.user.last_name} ${state.user.first_name} ${state.user.patronymic}`
    },
    getShortFIO: (state) => {
      return `${state.user.last_name} ${state.user.first_name.substr(0, 1)}.${state.user.patronymic.substr(0, 1)}`
    },
  },
  mutations: {
    setBackendStatus(state, data) {
      state.API_status = data
    },
    setAuthData(state, data) {
      state.user = data.data.user
      state.authStatus = true
      state.role = data.data.user.role
      if (data.data.pk) {
        state.pk = data.data.pk
      }
      if (data.data.user.role == "Student") {
        state.group = data.data.group
      } else {
        state.position = data.data.position
      }
    },
    updateAuthData(state, data) {
      state.pk = data.pk
      state.authStatus = true
      state.user = data.user
      state.role = data.user.role
      if (state.role == "Student") {
        state.group =  data.group
      } else {
        state.position = data.position
      }
    },
    removeUserData(state) {
      state.authStatus = false
      state.user = ""
      state.group = ""
      state.position = ""
    },
    addToken(state, token) {
      localStorage.setItem("token", token)
      state.token = token
    },
    addDisabled(state, data) {
      state.links.forEach((link, i) => {
        if (link.link == data) {
          state.links[i].disabled = true
        } else {
          state.links[i].disabled = false
        }
      })
    },
    updateSubjects(state, data) {
      state.subjects = data.sort((a, b) => (a.semestr > b.semestr ? 1 : -1))
    },
  },
  actions: {
    checkBackendStatus({ commit }) {
      return axios
        .get(API.status)
        .then(() => {
          commit("setBackendStatus", "success")
        })
        .catch(() => {
          setTimeout(() => {
            axios
              .get(API.status)
              .then(() => {
                commit("setBackendStatus", "success")
              })
              .catch(() => {
                commit("setBackendStatus", "error")
              })
          }, 2000)
        })
    },
    checkCurrentPage({ commit }, page) {
      if (page == "/") commit("addDisabled", page)
      else commit("addDisabled", page)
    },
    auth({ commit }, username) {
      axios
        .get(`${API.user}`, { params: { username: username } })
        .then((response) => {
          commit("setAuthData", response)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    createToken({ commit }, token) {
      commit("addToken", token)
    },
    removeUser({ commit }) {
      localStorage.removeItem("token")
      commit("removeUserData")
    },
    readToken({ commit }) {
      let token = localStorage.getItem("token")
      if (token != null) {
        axios
          .get(API.userMe, {
            headers: {
              Authorization: "Token " + token,
            },
          })
          .then((response) => {
            axios
              .get(`${API.user}`, { params: { username: response.data.username } })
              .then((response) => {
                commit("updateAuthData", response.data)
              })
              .catch((e) => {
                console.log(e)
              })
          })
          .catch((e) => {
            localStorage.removeItem("token")
            commit("removeUserData")
            console.log(e)
          })
      }
    },
    fetchSubjects({ commit, state }) {
      if (state.role == "Student") {
        return axios
          .get(`${API.getSubjects}`, {
            params: {
              username: state.user.user.username,
              role: state.role,
              group: state.group.name,
            },
          })
          .then((response) => {
            commit("updateSubjects", response.data)
          })
          .catch((e) => {
            console.log(e)
          })
      } else {
        return axios
          .get(`${API.getSubjects}`, {
            params: {
              username: state.user.user.username,
              role: state.role,
            },
          })
          .then((response) => {
            commit("updateSubjects", response.data)
          })
          .catch((e) => {
            console.log(e)
          })
      }
    },
  },
  modules: { docs, timetable, debts, exam, works, lections },
})
