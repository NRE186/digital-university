import axios from "axios"
import { API } from "@/constants"

export default {
  state: {
    worksScore: [],
    works: [],
  },
  getters: {
    getWorksScore: (state) => {
      return state.worksScore
    },
    getWorks: (state) => {
      return state.works
    },
  },
  actions: {
    fetchWorksScore({ commit }, data) {
      return axios
        .get(`${API.getWorks}`, {
          params: {
            group: data.group,
            subject: data.subject,
          },
        })
        .then((response) => {
          commit("setWorksScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    updateWorkMark({ commit }, data) {
      axios
        .get(`${API.updateWorkMark}`, {
          params: {
            pk: data.pk,
            mark: data.mark,
          },
        })
        .then((response) => {
          commit("updateWorkScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    createWork({ commit }, data) {
      return axios
        .post(`${API.createWork}`, {
          name: data.name,
          subject: data.subject,
          work_date: data.work_date,
          work_type: data.work_type,
          description: data.description,
        })
        .then((response) => {
          commit("updateWorkScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    updateWork({ commit }, data) {
      return axios
        .post(`${API.updateWork}`, {
          pk: data.pk,
          name: data.name,
          work_date: data.work_date,
          work_type: data.work_type,
          description: data.description,
        })
        .then((response) => {
          commit("updateWorkScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
  },
  mutations: {
    setWorksScore(state, data) {
      state.worksScore = data
      let res = []
      data.forEach((el) => {
        res.push({
          pk: el.work.pk,
          name: el.work.name,
          description: el.work.description,
          subject: el.work.subject,
          work_date: el.work.work_date,
          work_type: el.work.work_type,
          editable: false,
        })
      })
      let unique = Array.from(new Set(res.map(JSON.stringify))).map(JSON.parse)
      state.works = unique
    },
    updateWorkScore(state, data) {
      state.worksScore = data
    },
  },
}
