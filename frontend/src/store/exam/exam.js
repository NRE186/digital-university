import axios from "axios"
import { API } from "@/constants"

export default {
  state: {
    finalScore: [],
  },
  getters: {
    getFinalScore: (state) => {
      return state.finalScore
    },
  },
  actions: {
    fetchFinalScore({ commit }, data) {
      return axios
        .get(`${API.getFinalScoreBySubjectAndGroup}`, {
          params: {
            group: data.group,
            subject: data.subject,
          },
        })
        .then((response) => {
          commit("setFinalScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
    updateMark({ commit }, data) {
      axios
        .get(`${API.updateFinalScore}`, {
          params: {
            pk: data.pk,
            mark: data.mark,
          },
        })
        .then((response) => {
          commit("updateFinalScore", response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },
  },
  mutations: {
    setFinalScore(state, data) {
      state.finalScore = data
    },
    updateFinalScore(state, data) {
      state.finalScore = data
    },
  },
}
