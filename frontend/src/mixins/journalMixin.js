export default {
  methods: {
    // Методы для Lection*Content
    getLectionPercentage(data) {
      let length = data.length
      let iter = 0
      data.forEach((el) => {
        if (el.score == "Был") {
          iter++
        }
      })
      return `${Math.floor((iter / length) * 100)}%`
    },
    getWorkPercentage(data) {
      let length = data.length
      let iter = 0
      data.forEach((el) => {
        if (el.score == "Сдал" || el.score == "Отлично") {
          iter++
        }
        else if(el.score == "Хорошо"){
          iter += 0.7
        }
        else if(el.score == "Удовлетворительно"){
          iter += 0.5
        }
        else if(el.score == "Неудовлетворительно"){
          iter += 0.2
        }
        else{
          iter += 0
        }
      })
      return `${Math.floor((iter / length) * 100)}%`
    },
    formatedResult: (result) => {
      return result == "-" ? `Оценка не выставлена` : result
    },
    getFIO(user) {
      return `${user.last_name} ${user.first_name} ${user.patronymic}`
    },
    unique(arr) {
      let result = []

      for (let str of arr) {
        if (!result.includes(str)) {
          result.push(str)
        }
      }

      return result
    },
    getLectionsByStudent(student, arr) {
      return Object.values(arr).filter((item) => {
        return item.student == student
      })
    },
    // Методы для Work*Content
    getWorksByStudent(student, arr) {
      return Object.values(arr).filter((item) => {
        return item.student == student
      })
    },
  },
}
