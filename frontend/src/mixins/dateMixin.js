import counter from "@/utils/counter.js"

export default {
  methods: {
    getDate: (date) => {
      let D = new Date(date)
      let day = D.getDate() < 10 ? `0${D.getDate()}` : D.getDate()
      let month = D.getMonth() + 1 < 10 ? `0${D.getMonth() + 1}` : D.getMonth() + 1
      return `${day}.${month}`
    },
    getFullDate: (date) => {
      let D = new Date(date)
      let day = D.getDate() < 10 ? `0${D.getDate()}` : D.getDate()
      let month = D.getMonth() + 1 < 10 ? `0${D.getMonth() + 1}` : D.getMonth() + 1
      return `${day}.${month}.${D.getFullYear()}`
    },
    checkDate(date) {
      const currDate = new Date()
      currDate.setHours(0, 0, 0, 0)
      const workDate = new Date(date)
      workDate.setHours(0, 0, 0, 0)
      return currDate <= workDate ? "greater" : "less"
    },
    leftTime(date) {
      const currDate = new Date()
      currDate.setHours(0, 0, 0, 0)
      const workDate = new Date(date)
      workDate.setHours(0, 0, 0, 0)
      if (currDate <= workDate) {
        return `${Math.ceil((workDate - currDate) / 8.64e7)} ${counter(Math.ceil((workDate - currDate) / 8.64e7), [
          "день",
          "дня",
          "дней",
        ])} `
      } else {
        return `${Math.ceil((currDate - workDate) / 8.64e7)} ${counter(Math.ceil((currDate - workDate) / 8.64e7), [
          "день",
          "дня",
          "дней",
        ])} `
      }
    },
  },
}
