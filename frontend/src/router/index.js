import Vue from "vue"
import VueRouter from "vue-router"
import store from "@/store"

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: "Main",
    component: () => import(/* webpackChunkName: "main" */ "../views/Main.vue"),
  },
  {
    path: "/timetable",
    name: "Timetable",
    component: () => import(/* webpackChunkName: "timetable" */ "../views/Timetable.vue"),
  },
  {
    path: "/documents",
    name: "Documents",
    component: () => import(/* webpackChunkName: "docs" */ "../views/Documents.vue"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import(/* webpackChunkName: "login" */ "../views/Login.vue"),
    beforeEnter(to, from, next) {
      if (localStorage.getItem("token") == null) {
        next()
      } else {
        next("/")
      }
    },
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: () => import(/* webpackChunkName: "dashboard" */ "../views/Dashboard.vue"),
    beforeEnter(to, from, next) {
      if (localStorage.getItem("token") == null) {
        next(`/login?to=${to.path.replace("/", "")}`)
      } else {
        next()
      }
    },
  },
  {
    path: "/test",
    name: "TestData",
    component: () => import(/* webpackChunkName: "test" */ "../views/TestData.vue"),
  },
  {
    path: "/visit/:group/:subject",
    name: "Visit",
    component: () => import(/* webpackChunkName: "visit" */ "../views/Visit.vue"),
    props: true,
    beforeEnter(to, from, next) {
      if (localStorage.getItem("token") == null) {
        next(`/login?to=${to.path.replace("/", "")}`)
      } else {
        next()
      }
    },
  },
  {
    path: "/works/:group/:subject",
    name: "Works",
    component: () => import(/* webpackChunkName: "works" */ "../views/Works.vue"),
    props: true,
    beforeEnter(to, from, next) {
      if (localStorage.getItem("token") == null) {
        next(`/login?to=${to.path.replace("/", "")}`)
      } else {
        next()
      }
    },
  },
  {
    path: "/exam/:group/:subject",
    name: "Exam",
    component: () => import(/* webpackChunkName: "exam" */ "../views/Exam.vue"),
    props: true,
    beforeEnter(to, from, next) {
      if (localStorage.getItem("token") == null) {
        next(`/login?to=${to.path.replace("/", "")}`)
      } else {
        next()
      }
    },
  },
  {
    path: "*",
    name: "NotFound",
    component: () => import(/* webpackChunkName: "not-found" */ "../views/NotFound.vue"),
  },
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  if (to.path == "/" && from.path == "/" && localStorage.getItem("token") != null) {
    next("/dashboard")
  } else {
    next()
  }
  store.dispatch("checkCurrentPage", to.path)
})

export default router
