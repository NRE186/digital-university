//const URL = "http://127.0.0.1:8000"
const URL = "https://digital-university.herokuapp.com"

export const API = {
  userMe: `${URL}/users/me`,
  tokenLogin: `${URL}/token/login`,
  status: `${URL}/api/v1/status`,
  user: `${URL}/api/v1/user`,
  getSubjects: `${URL}/api/v1/getSubjects/`,
  getFinalScoreBySubjectAndGroup: `${URL}/api/v1/getFinalScoreBySubjectAndGroup/`,
  updateFinalScore: `${URL}/api/v1/updateFinalScore/`,
  getWorks: `${URL}/api/v1/getWorksBySubjectAndGroup/`,
  updateWorkMark: `${URL}/api/v1/updateWorkMark/`,
  getLections: `${URL}/api/v1/getLectionsBySubjectAndGroup/`,
  updateLectionMark: `${URL}/api/v1/updateLectionMark/`,
  createLection: `${URL}/api/v1/createLection/`,
  createWork: `${URL}/api/v1/createWork/`,
  updateLection: `${URL}/api/v1/updateLection/`,
  updateWork: `${URL}/api/v1/updateWork/`,
  getDebt: `${URL}/api/v1/getStudentDebt/`,
}
