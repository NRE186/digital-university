from bs4 import BeautifulSoup as bs
import requests
import json

url = "http://www.surgu.ru/ucheba/raspisanie/ochnaya-forma-obucheniya"
final_url = "https://www.surgu.ru"

html = requests.get(url)
soup = bs(html.content, "html.parser")
trList = [tr for tr in soup.find_all("tr")]

institutes = ['Институт государства и права', 'Институт гуманитарного образования и спорта',
              'Институт естественных и технических наук', 'Институт экономики и управления',
              'Медицинский институт', 'Политехнический институт']
current_institute = None
grade_type = "Бакалавриат, Специалитет"

results = []

for elem in trList[1:]:
    elemText = elem.getText().replace("\n", "")
    if elemText in institutes:
        current_institute = elemText
        continue
    if elemText == "Магистратура":
        grade_type = "Магистратура"
        continue
    results.append(
        {
            "name": elemText,
            "grade_type": grade_type,
            "institute": current_institute,
            "link": f"{final_url}{elem.find('a').get('href')}"
        }
    )

with open("frontend/src/store/timetable/timetable.json", "w", encoding="utf8") as write_file:
    json.dump(results, write_file, ensure_ascii=False, indent=4)
