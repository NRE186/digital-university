from django.contrib.auth.models import User
from rest_framework import serializers

from .models import userProfile


class MyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'email')


class UserProfileSerializer(serializers.ModelSerializer):
    user = MyUserSerializer()

    class Meta:
        model = userProfile
        fields = ('user', 'first_name',
                  'last_name', 'patronymic', 'role')


class ShortUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = userProfile
        fields = ('first_name',
                  'last_name', 'patronymic')
