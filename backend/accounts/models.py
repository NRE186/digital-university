from django.db import models
from django.contrib.auth.models import User

ROLES = (
    ('Student', 'Студент'),
    ('Teacher', 'Преподаватель'),
)


class userProfile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, related_name="profile")
    role = models.CharField(max_length=300, choices=ROLES)
    first_name = models.CharField(
        max_length=300, verbose_name=("Имя"))
    last_name = models.CharField(
        max_length=300, verbose_name=("Фамилия"))
    patronymic = models.CharField(
        max_length=300, verbose_name=("Отчество"))

    def __str__(self):
        return f'{self.last_name} {self.first_name} {self.patronymic}'
