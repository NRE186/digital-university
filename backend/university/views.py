from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, generics
from rest_framework.response import Response
from rest_framework.decorators import action
from .models import *
from .serializers import *
import json
from datetime import datetime


class UserDataViewSet(viewsets.ViewSet):
    def list(self, request, **kwargs):
        username = request.query_params.get('username')
        queryset = Student.objects.filter(
            user__user__username=username).first()
        if queryset:
            serializer = StudentSerializer(queryset)
        else:
            queryset = Teacher.objects.filter(
                user__user__username=username).first()
            if not queryset:
                raise Http404

            serializer = TeacherSerializer(queryset)
        return Response(serializer.data)


class CheckStatus(viewsets.ViewSet):
    def list(self, request, **kwargs):
        return Response({"status": "OK"})


class GetSubjects(viewsets.ViewSet):
    def list(self, request, **kwargs):
        username = request.query_params.get('username')
        role = request.query_params.get('role')
        if role == "Student":
            group = request.query_params.get('group')
            queryset = Subject.objects.filter(
                group__name=group)
        else:
            queryset = Subject.objects.filter(
                teacher__user__user__username=username)

        if queryset:
            serializer = SubjectSerializer(queryset, many=True)
        return Response(serializer.data)


class GetFinalScoreBySubjectAndGroup(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        subject = request.query_params.get('subject')
        group = request.query_params.get('group')
        group_q = StudentsGroup.objects.filter(
            name=group).first()
        subject_q = Subject.objects.filter(name=subject).first()
        students_q = Student.objects.filter(
            group__name=group).values_list('pk', flat=True)
        res = []
        for student in students_q:
            try:
                obj = FinalScore.objects.get(
                    student__pk=student, subject__name=subject)
                res.append(obj)
            except FinalScore.DoesNotExist:
                obj = FinalScore(group=group_q, semestr=subject_q.semestr,
                                 student=Student.objects.get(pk=student), subject=subject_q, score='-')
                obj.save()
                res.append(obj)

        serializer = FinalScoreSerializer(res, many=True)

        return Response(serializer.data)


class UpdateFinalScore(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        pk = request.query_params.get('pk')
        mark = request.query_params.get('mark')
        FinalScore.objects.filter(pk=pk).update(score=mark)
        return Response({"status": "OK"})


class GetWorkBySubjectAndGroup(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        subject = request.query_params.get('subject')
        group = request.query_params.get('group')
        group_q = StudentsGroup.objects.filter(
            name=group).first()
        subject_q = Subject.objects.filter(name=subject).first()
        students_q = Student.objects.filter(
            group__name=group).values_list('pk', flat=True)
        works_q = Work.objects.filter(subject__name=subject)
        res = []
        for student in students_q:
            for work in works_q:
                try:
                    obj = PracticalScore.objects.get(
                        student__pk=student, subject__name=subject, work=work)
                    res.append(obj)
                except PracticalScore.DoesNotExist:
                    obj = PracticalScore(group=group_q, student=Student.objects.get(
                        pk=student), subject=subject_q, work=work, score='Не сдал')
                    obj.save()
                    res.append(obj)

        serializer = PracticalScoreSerializer(res, many=True)

        return Response(serializer.data)


class UpdateWorkMark(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        pk = request.query_params.get('pk')
        mark = request.query_params.get('mark')
        PracticalScore.objects.filter(pk=pk).update(score=mark)
        return Response({"status": "OK"})


class GetLectionBySubjectAndGroup(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        subject = request.query_params.get('subject')
        group = request.query_params.get('group')
        group_q = StudentsGroup.objects.filter(
            name=group).first()
        subject_q = Subject.objects.filter(name=subject).first()
        students_q = Student.objects.filter(
            group__name=group).values_list('pk', flat=True)
        lections_q = Lection.objects.filter(subject__name=subject)
        res = []
        for student in students_q:
            for lection in lections_q:
                try:
                    obj = LectionScore.objects.get(
                        student__pk=student, subject__name=subject, lection=lection)
                    res.append(obj)
                except LectionScore.DoesNotExist:
                    obj = LectionScore(group=group_q, student=Student.objects.get(
                        pk=student), subject=subject_q, lection=lection, score='Был')
                    obj.save()
                    res.append(obj)

        serializer = LectionScoreSerializer(res, many=True)

        return Response(serializer.data)


class UpdateLectionMark(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        pk = request.query_params.get('pk')
        mark = request.query_params.get('mark')
        LectionScore.objects.filter(pk=pk).update(score=mark)
        return Response({"status": "OK"})


class CreateLection(viewsets.ViewSet):
    def create(self, request, *args, **kwargs):
        name = request.data["name"]
        subject = request.data["subject"]
        lection_date = datetime.strptime(
            request.data["lection_date"], "%Y-%m-%d")
        description = request.data["description"]
        subject_q = Subject.objects.filter(name=subject).first()
        obj = Lection(name=name, subject=subject_q,
                      lection_date=lection_date, description=description)
        obj.save()
        return Response({"status": "OK"})


class CreateWork(viewsets.ViewSet):
    def create(self, request, *args, **kwargs):
        name = request.data["name"]
        subject = request.data["subject"]
        work_date = datetime.strptime(
            request.data["work_date"], "%Y-%m-%d")
        description = request.data["description"]
        work_type = request.data["work_type"]
        subject_q = Subject.objects.filter(name=subject).first()
        obj = Work(name=name, subject=subject_q,
                   work_date=work_date, work_type=work_type, description=description)
        obj.save()
        return Response({"status": "OK"})


class UpdateWork(viewsets.ViewSet):
    def create(self, request, *args, **kwargs):
        pk = request.data["pk"]
        name = request.data["name"]
        work_date = datetime.strptime(
            request.data["work_date"], "%Y-%m-%d")
        description = request.data["description"]
        work_type = request.data["work_type"]
        Work.objects.filter(pk=pk).update(
            name=name, work_date=work_date, description=description, work_type=work_type)
        return Response({"status": "OK"})


class UpdateLection(viewsets.ViewSet):
    def create(self, request, *args, **kwargs):
        pk = request.data["pk"]
        name = request.data["name"]
        lection_date = datetime.strptime(
            request.data["lection_date"], "%Y-%m-%d")
        description = request.data["description"]
        Lection.objects.filter(pk=pk).update(
            name=name, lection_date=lection_date, description=description)
        return Response({"status": "OK"})


class GetStudentDebt(viewsets.ViewSet):
    def list(self, request, *args, **kwargs):
        student_pk = request.query_params.get('pk')
        group = request.query_params.get('group')
        group_q = StudentsGroup.objects.filter(name=group).first()
        subject_q = Subject.objects.filter(group=group_q)
        tmp = []
        for subject in subject_q:
            obj = FinalScore.objects.get(
                student__pk=student_pk, subject__name=subject)
            tmp.append(obj)
        res = [{"subject": r.subject.name, "score": r.score} for r in tmp]
        return Response(res)
