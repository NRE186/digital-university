from django.forms import ModelChoiceField
from django.contrib import admin

from .models import *
from accounts.models import userProfile


class StudentAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'user':
            return ModelChoiceField(userProfile.objects.filter(role='Student'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class TeacherAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'user':
            return ModelChoiceField(userProfile.objects.filter(role='Teacher'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Student, StudentAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(StudentsGroup)
admin.site.register(Subject)
admin.site.register(Lection)
admin.site.register(LectionScore)
admin.site.register(Work)
admin.site.register(PracticalScore)
admin.site.register(FinalScore)
