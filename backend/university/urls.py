from rest_framework import routers
from .views import *

router = routers.DefaultRouter()
router.register(r'user',
                UserDataViewSet, basename='userInfo')
router.register(r'status',
                CheckStatus, basename='statusInfo')
router.register(r'getSubjects',
                GetSubjects, basename='getSubject')
router.register(r'getFinalScoreBySubjectAndGroup',
                GetFinalScoreBySubjectAndGroup, basename='getFinalScoreBySubjectAndGroup')
router.register(r'updateFinalScore',
                UpdateFinalScore, basename='updateFinalScore')
router.register(r'getWorksBySubjectAndGroup',
                GetWorkBySubjectAndGroup, basename='getWorksBySubjectAndGroup')
router.register(r'updateWorkMark',
                UpdateWorkMark, basename='updateWorkMark')
router.register(r'getLectionsBySubjectAndGroup',
                GetLectionBySubjectAndGroup, basename='getLectionsBySubjectAndGroup')
router.register(r'updateLectionMark',
                UpdateLectionMark, basename='updateLectionMark')
router.register(r'createLection',
                CreateLection, basename='createLection')
router.register(r'createWork',
                CreateWork, basename='createWork')
router.register(r'updateLection',
                UpdateLection, basename='updateLection')
router.register(r'updateWork',
                UpdateWork, basename='updateWork')
router.register(r'getStudentDebt', GetStudentDebt, basename='getStudentDeb')


university_urlpatterns = [] + router.urls
