import datetime

from django.db import models
from django.utils.translation import gettext_lazy as _

from accounts.models import userProfile

SEMESTRS = (
    ('1', '1 семестр'),
    ('2', '2 семестр'),
    ('3', '3 семестр'),
    ('4', '4 семестр'),
    ('5', '5 семестр'),
    ('6', '6 семестр'),
    ('7', '7 семестр'),
    ('8', '8 семестр'),
)

FINAL = (
    ('Зачёт', 'Зачёт'),
    ('Зачёт с оценкой', 'Зачёт с оценкой'),
    ('Экзамен', 'Экзамен'),
)

PRACTICAL_MARKS = (
    ('Не сдал', 'Не сдал'),
    ('Отлично', 'Отлично'),
    ('Хорошо', 'Хорошо'),
    ('Удовлетворительно', 'Удовлетворительно'),
    ('Неудовлетворительно', 'Неудовлетворительно'),
    ('Сдал', 'Сдал'),
)

LESSON_MARKS = (
    ('Не был', 'Не был'),
    ('Был', 'Был'),
)

FINAL_MARKS = (
    ('-', '-'),
    ('Не зачёт', 'Не зачёт'),
    ('Зачёт', 'Зачёт'),
    ('Отлично', 'Отлично'),
    ('Хорошо', 'Хорошо'),
    ('Удовлетворительно', 'Удовлетворительно'),
    ('Неудовлетворительно', 'Неудовлетворительно'),
)

WORK_TYPES = ((
    ('Лабораторная работа', 'Лабораторная работа'),
    ('Контрольная работа', 'Контрольная работа'),
    ('Тест', 'Тест'),
    ('Самостоятельная работа', 'Самостоятельная работа'),
))


class StudentsGroup(models.Model):
    class Meta:
        verbose_name = "Группа студентов"
        verbose_name_plural = "Список групп"

    name = models.CharField(max_length=300, verbose_name="Номер группы")
    direction = models.CharField(max_length=300, verbose_name="Направление")

    def __str__(self):
        return f"Группа {self.name}"


class Student(models.Model):
    class Meta:
        verbose_name = "Студент"
        verbose_name_plural = "Список студентов"

    user = models.OneToOneField(
        userProfile,
        on_delete=models.CASCADE,
        primary_key=True,
        verbose_name="Пользователь"
    )
    group = models.ForeignKey(
        StudentsGroup, on_delete=models.CASCADE, verbose_name="Группа")

    def __str__(self):
        return f"{self.group.name} - {self.user.last_name} {self.user.first_name} {self.user.patronymic}"


class Teacher(models.Model):
    class Meta:
        verbose_name = "Преподаватель"
        verbose_name_plural = "Список преподавателей"

    user = models.OneToOneField(
        userProfile,
        on_delete=models.CASCADE,
        primary_key=True,
        verbose_name="Пользователь"
    )
    position = models.CharField(max_length=300, verbose_name='Должность')

    def __str__(self):
        return f"{self.position} - {self.user.last_name} {self.user.first_name} {self.user.patronymic}"


class Subject(models.Model):
    class Meta:
        verbose_name = "Дисциплина"
        verbose_name_plural = "Список дисциплин"

    name = models.CharField(max_length=300, verbose_name="Название дисциплины")
    teacher = models.ForeignKey(
        Teacher, on_delete=models.CASCADE, verbose_name="Преподаватель")
    group = models.ForeignKey(
        StudentsGroup, on_delete=models.CASCADE, verbose_name="Группа")
    semestr = models.CharField(
        max_length=300, choices=SEMESTRS, verbose_name="Семестр")
    final_result = models.CharField(
        max_length=300, choices=FINAL, verbose_name="Итоговый результат дисциплины")

    def __str__(self):
        return self.name


class Work(models.Model):
    class Meta:
        verbose_name = "Практическая работа"
        verbose_name_plural = "Список практических работ"
        unique_together = ['name', 'subject', 'file_work']

    name = models.CharField(
        max_length=300, verbose_name="Название практической работы")
    subject = models.ForeignKey(
        Subject, on_delete=models.CASCADE, verbose_name="Дисциплина")
    work_date = models.DateField(_("Date"), default=datetime.date.today)
    work_type = models.CharField(
        max_length=300, choices=WORK_TYPES, verbose_name="Тип практической работы")
    description = models.TextField(
        max_length=20000, verbose_name="Описание работы")
    file_work = models.FileField(verbose_name="Документ с работой", blank=True)

    def __str__(self):
        return f"{self.work_type} - {self.name} за {self.work_date}"


class PracticalScore(models.Model):
    class Meta:
        verbose_name = "Журнал выполнения практических работ"
        verbose_name_plural = "Список журналов выполнения практических работ"
        unique_together = ['student', 'group', 'subject', 'work']

    group = models.ForeignKey(
        StudentsGroup, on_delete=models.CASCADE, verbose_name="Группа")
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, verbose_name="Студент")
    subject = models.ForeignKey(
        Subject, on_delete=models.CASCADE, verbose_name="Дисциплина")
    work = models.ForeignKey(
        Work, on_delete=models.CASCADE, verbose_name="Практическая работа")
    score = models.CharField(
        max_length=100, choices=PRACTICAL_MARKS, verbose_name="Оценка за работу")

    def __str__(self):
        return f'Журнал выполнения практических работ по дисциплине {self.subject} для группы {self.group} на {self.subject.semestr} семестр'


class Lection(models.Model):
    class Meta:
        verbose_name = "Лекция"
        verbose_name_plural = "Список лекций"
        unique_together = ['name', 'subject']

    name = models.CharField(
        max_length=300, verbose_name="Название лекции")
    subject = models.ForeignKey(
        Subject, on_delete=models.CASCADE, verbose_name="Дисциплина")
    lection_date = models.DateField(_("Date"), default=datetime.date.today)
    description = models.TextField(
        max_length=20000, verbose_name="Описание лекции")

    def __str__(self):
        return f"{self.name} по дипсциплине {self.subject.name} за {self.lection_date}"


class LectionScore(models.Model):
    class Meta:
        verbose_name = "Журнал посещения лекций"
        verbose_name_plural = "Список журналов посещения лекций"
        unique_together = ['student', 'group', 'subject', 'lection']

    group = models.ForeignKey(
        StudentsGroup, on_delete=models.CASCADE, verbose_name="Группа")
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, verbose_name="Студент")
    subject = models.ForeignKey(
        Subject, on_delete=models.CASCADE, verbose_name="Дисциплина")
    lection = models.ForeignKey(
        Lection, on_delete=models.CASCADE, verbose_name="Практическая работа")
    score = models.CharField(
        max_length=100, choices=PRACTICAL_MARKS, verbose_name="Оценка за работу")

    def __str__(self):
        return f'Журнал посещения лекций по дисциплине {self.subject} для группы {self.group} на {self.subject.semestr} семестр'


class FinalScore(models.Model):
    class Meta:
        verbose_name = "Зачётная ведомость"
        verbose_name_plural = "Зачётные ведомости"
        unique_together = ['student', 'group', 'subject', 'semestr']

    group = models.ForeignKey(
        StudentsGroup, on_delete=models.CASCADE, verbose_name="Группа")
    semestr = models.CharField(
        max_length=300, choices=SEMESTRS, verbose_name="Семестр")
    student = models.ForeignKey(
        Student, on_delete=models.CASCADE, verbose_name="Студент")
    subject = models.ForeignKey(
        Subject, on_delete=models.CASCADE, verbose_name="Дисциплина")
    score = models.CharField(
        max_length=100, choices=FINAL_MARKS, verbose_name="Оценка за работу")

    def __str__(self):
        return f'Зачетная ведомость по дисциплине {self.subject} для студента {self.student} на {self.semestr} семестр'
