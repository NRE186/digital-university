from rest_framework import serializers

from .models import *
from accounts.serializers import UserProfileSerializer, ShortUserProfileSerializer


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentsGroup
        fields = ['name', 'direction']


class StudentSerializer(serializers.ModelSerializer):
    user = UserProfileSerializer()
    group = GroupSerializer()

    class Meta:
        model = Student
        fields = ['pk', 'user', 'group']


class TeacherSerializer(serializers.ModelSerializer):
    user = UserProfileSerializer()

    class Meta:
        model = Teacher
        fields = ['user', 'position']


class ShortStudentSerializer(serializers.ModelSerializer):
    user = ShortUserProfileSerializer()

    class Meta:
        model = Student
        fields = ['user']


class ShortTeacherSerializer(serializers.ModelSerializer):
    user = ShortUserProfileSerializer()

    class Meta:
        model = Teacher
        fields = ['user', 'position']


class SubjectSerializer(serializers.ModelSerializer):
    teacher = ShortTeacherSerializer()
    group = GroupSerializer()

    class Meta:
        model = Subject
        fields = ['name', 'teacher', 'group', 'semestr', 'final_result']


class FinalScoreSerializer(serializers.ModelSerializer):
    student = ShortStudentSerializer()
    subject = SubjectSerializer()

    class Meta:
        model = FinalScore
        fields = ['pk', 'student', 'subject', 'score']


class WorkSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer()

    class Meta:
        model = Work
        fields = ['pk', 'name', 'subject', 'work_date',
                  'work_type', 'description', 'file_work']


class PracticalScoreSerializer(serializers.ModelSerializer):
    group = GroupSerializer()
    student = ShortStudentSerializer()
    subject = SubjectSerializer()
    work = WorkSerializer()

    class Meta:
        model = PracticalScore
        fields = ['pk', 'group', 'student', 'subject', 'work', 'score']


class LectionSerializer(serializers.ModelSerializer):
    subject = SubjectSerializer()

    class Meta:
        model = Lection
        fields = ['pk', 'name', 'subject', 'lection_date', 'description', ]


class LectionScoreSerializer(serializers.ModelSerializer):
    group = GroupSerializer()
    student = ShortStudentSerializer()
    subject = SubjectSerializer()
    lection = LectionSerializer()

    class Meta:
        model = LectionScore
        fields = ['pk', 'group', 'student', 'subject', 'lection', 'score']
