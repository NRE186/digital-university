# Использование авторизации на токенах

## Получение токена

1. Отправить на страницу token/login JSON в формате

```json
{
  "username": "username",
  "password": "password"
}
```

2. В ответ придёт токен

```json
{
  "auth_token": "dd7cfbff8525727b267411c692d08ee34478f2af"
}
```

## Узнать, какой пользователь авторизовался

1. Отправить на страницу users/me/ Authorization заголовок в формате

> Authorization: Token dd7cfbff8525727b267411c692d08ee34478f2af

2. В ответ придёт JSON с информацией о пользователе

```json
{
  "email": "nre186@vk.com",
  "id": 2,
  "username": "root"
}
```
