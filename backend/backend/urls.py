from django.conf import settings
from django.conf.urls import include
from django.views.static import serve
from django.contrib import admin
from django.urls import path
from accounts.urls import accounts_urlpatterns
from university.urls import university_urlpatterns

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Digital University REST API endpoints",
        default_version='v1',
        description="This is Redoc documentation for Digital University REST API endpoints",
        terms_of_service="https://www.google.com/policies/terms/",
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path(r'^swagger(?P<format>\.json|\.yaml)$',
        schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path(r'^swagger/$', schema_view.with_ui('swagger',
        cache_timeout=0), name='schema-swagger-ui'),
    path(r'^redoc/$', schema_view.with_ui('redoc',
        cache_timeout=0), name='schema-redoc'),
    path(r'^api/v1/', include(accounts_urlpatterns)),
    path(r'^api/v1/', include(university_urlpatterns)),
    path("admin/", admin.site.urls),
    path(r'', include('djoser.urls')),
    path(r'', include('djoser.urls.authtoken')),
]

if settings.DEBUG:
    urlpatterns += [
        path(r'files/(?P<path>.*)$', serve, {
            'document_root': settings.MEDIA_ROOT
        })
    ]
