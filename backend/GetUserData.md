# Получение данных о пользователе

## Получаем информацию о пользователе в модели User, если он авторизован и имеет auth token(если нет, [то](AuthTokenUsage.md))

1. Отправить на страницу api/v1/users/me/ Authorization заголовок в формате

> Authorization: Token dd7cfbff8525727b267411c692d08ee34478f2af

2. В ответ придёт JSON с информацией о пользователе

```json
{
  "email": "nre186@vk.com",
  "id": 2,
  "username": "root"
}
```

3. Отправить на страницу api/v1/user/{username} запрос для получения информации о пользователе в зависимости от его роли

> Пример для пользователя admin - url = /api/v1/user/admin/ с ролью студента

```json
{
  "user": {
    "user": {
      "username": "NRE",
      "email": "nre@vk.com"
    },
    "first_name": "Александр",
    "last_name": "Прокофьев",
    "patronymic": "Андреевич",
    "role": "Student"
  },
  "group": {
    "name": "607-81",
    "direction": "Информационные системы и технологии"
  }
}
```

> Пример для пользователя NRE - url = /api/v1/user/NRE/ с ролью преподавателя

```json
{
  "user": {
    "user": {
      "username": "NRE",
      "email": "email@gmail.com"
    },
    "first_name": "Иван",
    "last_name": "Иванов",
    "patronymic": "Иванович",
    "role": "Teacher"
  },
  "position": "Доцент кафедры информатики и вычислительной техники"
}
```
