# Диаграмма компонентов

![image.png](./Images/Components.png)

# Диаграмма классов

![image.png](./Images/classes.png)

# DFD

![image.png](./Images/DFD.png)

# IDEF0

![image.png](./Images/IDEF0.png)

# IDEF1X

![image.png](./Images/IDEF1X.png)

# BPMN

## Роль - администратор ИС

![image.png](./Images/admin_bpmn.svg)

## Роль - студент

![image.png](./Images/student.svg)

## Роль - преподаватель

![image.png](./Images/teacher.svg)
