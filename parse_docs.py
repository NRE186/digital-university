from bs4 import BeautifulSoup as bs
import requests
import json
from urllib.parse import unquote
from progress.spinner import Spinner
import time
import re


start = time.time()
url = "http://publish.surgu.ru/bsm/"
spinner = Spinner(f'Парсим данные с {url}  ')

html = requests.get(url)
soup = bs(html.content, "html.parser")
links = [link.get("href") for link in soup.find_all("a")]


def get_characteristic(page):
    results = []
    n_page = f"{page}1 ОБЩАЯ ХАРАКТЕРИСТИКА/"
    html = requests.get(n_page)
    soup = bs(html.content, "html.parser")
    for l in soup.find_all("a"):
        b = l.get("href")
        if b.find("../") == -1 and b.__contains__('.sig') == False:
            results.append(
                {
                    "name": unquote(b.replace("/", "")),
                    "link": unquote(f"{n_page}{b}")
                })

    return results


def get_calendar(page):
    results = []
    n_page = f"{page}2 КАЛЕНДАРНЫЙ ГРАФИК/"
    html = requests.get(n_page)
    soup = bs(html.content, "html.parser")
    for l in soup.find_all("a"):
        b = l.get("href")
        if b.find("../") == -1 and b.__contains__('.sig') == False:
            results.append(
                {
                    "name": unquote(b.replace("/", "")),
                    "link": unquote(f"{n_page}{b}")
                })

    return results


def get_plans(page):
    results = []
    n_page = f"{page}3 УЧЕБНЫЕ ПЛАНЫ/"
    html = requests.get(n_page)
    soup = bs(html.content, "html.parser")
    for l in soup.find_all("a"):
        b = l.get("href")
        if b.find("../") == -1 and b.__contains__('.sig') == False:
            results.append(
                {
                    "name": unquote(b.replace("/", "")),
                    "link": unquote(f"{n_page}{b}")
                })

    return results


def get_programs(page):
    results = []
    n_page = f"{page}4 РАБОЧИЕ ПРОГРАММЫ ДИСЦИПЛИН/"
    html = requests.get(n_page)
    soup = bs(html.content, "html.parser")
    for l in soup.find_all("a"):
        b = l.get("href")
        if b.find("../") == -1 and b.__contains__('.sig') == False:
            results.append(
                {
                    "name": unquote(b.replace("/", "")),
                    "link": unquote(f"{n_page}{b}")
                })

    return results


def get_practicum(page):
    results = []
    n_page = f"{page}6 ОТЧЁТЫ О ПРАКТИКАХ/"
    html = requests.get(n_page)
    soup = bs(html.content, "html.parser")
    for l in soup.find_all("a"):
        b = l.get("href")
        if b.find("../") == -1 and b.__contains__('.sig') == False:
            results.append(
                {
                    "name": unquote(b.replace("/", "")),
                    "link": unquote(f"{n_page}{b}")
                })

    return results


res = []
for link in links:
    spinner.next()
    html = requests.get(f"{url}{link}1")
    soup = bs(html.content, "html.parser")
    for l in soup.find_all("a"):
        a = l.get("href")
        new_url = f"{url}{link}1/{a}"
        if new_url.find("../") == -1:
            name = unquote(a.replace("/", ""))
            m = re.search('[0-9]+.[0-9]+.[0-9]+', new_url)
            code = m.group(0)
            fgos3p = unquote(f"{new_url}ФГОС ВО 3+/")
            fgos3pp = unquote(f"{new_url}ФГОС ВО 3++/")
            res.append({"name": name,
                        "code": code,
                        "value":
                        [
                            {
                                "name": "ФГОС ВО 3+",
                                "characteristic": get_characteristic(fgos3p),
                                "calendar": get_calendar(fgos3p),
                                "plans": get_plans(fgos3p),
                                "programs": get_programs(fgos3p),
                                "practicum": get_practicum(fgos3p),
                            },
                            {
                                "name": "ФГОС ВО 3++",
                                "characteristic": get_characteristic(fgos3pp),
                                "calendar": get_calendar(fgos3pp),
                                "plans": get_plans(fgos3pp),
                                "programs": get_programs(fgos3pp),
                                "practicum": get_practicum(fgos3pp),
                            }
                        ]
                        })


with open("frontend/src/store/docs/docs.json", "w", encoding='utf8') as write_file:
    json.dump(res, write_file, ensure_ascii=False)

end = time.time()
print()
print("Время выполнения скрипта", end - start)
