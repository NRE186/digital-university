# Digital University

Веб-сайт - личный кабинет студента и преподавателя. Студент сможет просматривать свою успеваемость, долги, посещения занятий, расписание, учебные планы и другие документы. Преподаватель сможет в удобном формате отмечать посещения студентов на занятиях, проставлять выполнение работ, а так же проставлять оценки за зачёты и экзамены.

## Документы

[Видение](Documents/vision.md)

[Модель предметной области](Documents/Images/unknown.png)

[Модель предметной области (полная)](Documents/Images/myapp_models.png)

[Словарь терминов](Documents/dictionary.md)

[Дополнительная спецификация](Documents/add_spec.md)

[Use Case Диаграмма](Documents/Images/use-case.png)

[Диаграмма последовательностей](Documents/sequence.md)

[Пример сгенерированной ведомости](Documents/example.pdf)

[Схемы, диаграммы](Documents/images.md)

## Проверка орфографии

`yaspeller -e ".md, .vue" . --ignore-capitalization`

## Ссылка на сайт

[Digital University](https://digital-university.tech/)
